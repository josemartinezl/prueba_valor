-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-11-2017 a las 16:20:42
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `valorca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detail_quotation`
--

CREATE TABLE `detail_quotation` (
  `package_id` int(11) UNSIGNED NOT NULL,
  `quotation_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detail_quotation`
--

INSERT INTO `detail_quotation` (`package_id`, `quotation_id`) VALUES
(2, 6),
(3, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1511899558),
('m171123_024953_create_product_table', 1511899561),
('m171123_031142_create_user_table', 1511899561),
('m171123_032148_create_package_table', 1511899561),
('m171123_032326_create_promotion_table', 1511899565),
('m171123_041801_create_quotation_table', 1511899568),
('m171123_044041_create_detail_quotation_table', 1511899573),
('m171128_192304_create_producto_quotation_table', 1511899577);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `package`
--

CREATE TABLE `package` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `package`
--

INSERT INTO `package` (`id`, `description`) VALUES
(1, 'Paquete navideño'),
(2, 'Paquete de verano'),
(3, 'Paquete de oferta especial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `name`, `type`) VALUES
(1, 'Mayonesa Nesa', 'Lacteos grasosos'),
(2, 'Pasta la Bologna', 'Pasta'),
(3, 'El dorado', 'Proteina'),
(4, 'Pasta Colgate', 'Higiente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_quotation`
--

CREATE TABLE `producto_quotation` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `quotation_id` int(11) UNSIGNED NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto_quotation`
--

INSERT INTO `producto_quotation` (`product_id`, `quotation_id`, `price`, `quantity`) VALUES
(1, 6, '1000.00', 50),
(3, 6, '10.00', 2000),
(3, 7, '5000.00', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `promotion`
--

CREATE TABLE `promotion` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `package_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `promotion`
--

INSERT INTO `promotion` (`product_id`, `package_id`, `quantity`, `price`, `discount`) VALUES
(1, 1, 50, 20, 9),
(2, 3, 20, 5000, 9),
(3, 2, 10, 500, 12),
(3, 3, 50, 9000, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotation`
--

CREATE TABLE `quotation` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) UNSIGNED DEFAULT NULL,
  `salesman_id` int(11) UNSIGNED DEFAULT NULL,
  `tax` decimal(10,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `quotation`
--

INSERT INTO `quotation` (`id`, `customer_id`, `salesman_id`, `tax`) VALUES
(6, 2, 2, '200.000'),
(7, 2, 1, '12.000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `ruc` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `ruc`, `name`, `last_name`) VALUES
(1, '321J3J', 'Jose ', 'Mallama'),
(2, 'ASDYT21', 'Silvia', 'Morales');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `detail_quotation`
--
ALTER TABLE `detail_quotation`
  ADD PRIMARY KEY (`package_id`,`quotation_id`),
  ADD KEY `idx-detail_quotation-quotation_id` (`quotation_id`),
  ADD KEY `idx-detail_quotation_package_id` (`package_id`);

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `producto_quotation`
--
ALTER TABLE `producto_quotation`
  ADD PRIMARY KEY (`product_id`,`quotation_id`),
  ADD KEY `idx-quotation-product_id` (`product_id`),
  ADD KEY `idx-quotation-quotation_id` (`quotation_id`);

--
-- Indices de la tabla `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`product_id`,`package_id`),
  ADD KEY `idx-promotion-product_id` (`product_id`),
  ADD KEY `idx-promotion-package_id` (`package_id`);

--
-- Indices de la tabla `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-quotation-customer_id` (`customer_id`),
  ADD KEY `idx-quotation-salesman_id` (`salesman_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ruc` (`ruc`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `package`
--
ALTER TABLE `package`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `detail_quotation`
--
ALTER TABLE `detail_quotation`
  ADD CONSTRAINT `idf-detail_quotation-quotation_id` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `idf-detail_quotation_package_id` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `producto_quotation`
--
ALTER TABLE `producto_quotation`
  ADD CONSTRAINT `idf-quotation-quotation_id` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `idf-quotation_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `promotion`
--
ALTER TABLE `promotion`
  ADD CONSTRAINT `idx-promotion-package_id` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `idx-promotion-product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `idf-quotation-customer_id` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `idf-quotation-salesman_id` FOREIGN KEY (`salesman_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

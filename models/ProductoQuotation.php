<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto_quotation".
 *
 * @property string $product_id
 * @property string $quotation_id
 * @property string $price
 * @property integer $quantity
 *
 * @property Quotation $quotation
 * @property Product $product
 */
class ProductoQuotation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producto_quotation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'quotation_id', 'price', 'quantity'], 'required'],
            [['product_id', 'quotation_id', 'quantity'], 'integer'],
            [['price'], 'number'],
            [['quotation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Quotation::className(), 'targetAttribute' => ['quotation_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Producto',
            'quotation_id' => 'Quotation ID',
            'price' => 'Precio',
            'quantity' => 'Cantidad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotation()
    {
        return $this->hasOne(Quotation::className(), ['id' => 'quotation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}

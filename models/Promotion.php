<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promotion".
 *
 * @property string $product_id
 * @property string $package_id
 * @property integer $quantity
 * @property double $price
 * @property double $discount
 *
 * @property Package $package
 * @property Product $product
 */
class Promotion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'package_id', 'quantity', 'price', 'discount'], 'required'],
            [['product_id', 'package_id', 'quantity'], 'integer'],
            [['price', 'discount'], 'number'],
            [['package_id'], 'exist', 'skipOnError' => true, 'targetClass' => Package::className(), 'targetAttribute' => ['package_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'package_id' => 'Package ID',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'discount' => 'Discount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}

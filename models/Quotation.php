<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation".
 *
 * @property string $id
 * @property string $customer_id
 * @property string $salesman_id
 * @property string $tax
 *
 * @property DetailQuotation[] $detailQuotations
 * @property Package[] $packages
 * @property ProductoQuotation[] $productoQuotations
 * @property Product[] $products
 * @property Users $customer
 * @property Users $salesman
 */
class Quotation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quotation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'salesman_id'], 'integer'],
            [['tax'], 'required'],
            [['tax'], 'number'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['salesman_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['salesman_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Numero de cotizacion',
            'customer_id' => 'Cliente',
            'salesman_id' => 'Vendedor',
            'tax' => 'Impuesto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailQuotations()
    {
        return $this->hasMany(DetailQuotation::className(), ['quotation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackages()
    {
        return $this->hasMany(Package::className(), ['id' => 'package_id'])->viaTable('detail_quotation', ['quotation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoQuotations()
    {
        return $this->hasMany(ProductoQuotation::className(), ['quotation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('producto_quotation', ['quotation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Users::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesman()
    {
        return $this->hasOne(Users::className(), ['id' => 'salesman_id']);
    }
}

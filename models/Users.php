<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property string $id
 * @property string $ruc
 * @property string $name
 * @property string $last_name
 *
 * @property Quotation[] $quotations
 * @property Quotation[] $quotations0
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ruc', 'name', 'last_name'], 'required'],
            [['ruc', 'name', 'last_name'], 'string', 'max' => 50],
            [['ruc'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ruc' => 'RUC',
            'name' => 'Nombre',
            'last_name' => 'Apellido',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotations()
    {
        return $this->hasMany(Quotation::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuotations0()
    {
        return $this->hasMany(Quotation::className(), ['salesman_id' => 'id']);
    }
}

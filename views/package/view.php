<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = $model->description;
$this->params['breadcrumbs'][] = ['label' => 'Paquetes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'description',
        ],
    ]) ?>

    <div class="grid-view">
        <div class="summary"><b><?= count($model->promotions) ?></b> producto(s) contenido(s).</div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Descripción</th>
                    <th>Tipo</th>
                    <th>Precio $</th>
                    <th>Cantidad</th>
                    <th>Descuento</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $index = 0;
                    $total = 0;
                    foreach ($model->promotions as $value) {
                        $total += ($value->price * $value->quantity) - (($value->price * $value->quantity) * $value->discount / 100);
                ?>
                    <tr>
                        <td>
                            <?= $index + 1 ?>
                        </td>
                        <td>
                            <?= $value->product->name ?>
                        </td>
                        <td>
                            <?= $value->product->type ?>
                        </td>
                        <td>
                            <?= $value->price ?>
                        </td>
                        <td>
                            <?= $value->quantity ?>
                        </td>
                        <td>
                            <?= $value->discount ?>
                        </td>
                    </tr>
                <?php
                        $index++;
                    }
                ?>
            </tbody>        
        </table>
    </div>

    <h3>Total <?= $total ?></h3>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\Users;
use app\models\Product;
use app\models\Package;
use wbraganca\dynamicform\DynamicFormWidget;
/* @var $this yii\web\View */
/* @var $model app\models\Quotation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="quotation-form">

    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>

   
    <?= $form->field($model,'customer_id')->widget(Select2::classname(), [
    			'data' => ArrayHelper::map(Users::find()->all(),'id','ruc'),
    			
    			'options' => ['placeholder' => 'Selecciona el cliente ...'],
    			'pluginOptions' => [
        		'allowClear' => true
    								],
				]); 

    ?>

    <?= $form->field($model, 'salesman_id')->widget(Select2::classname(), [
    			'data' => ArrayHelper::map(Users::find()->all(),'id','ruc'),
    			'options' => ['placeholder' => 'Seleccionar vendedor ...'],
    			'pluginOptions' => [
        		'allowClear' => true
    								],
				]); 

    ?>

    <?= $form->field($model, 'tax')->textInput(['maxlength' => true]) ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><i class="glyphicon glyphicon-envelope"></i> Productos</h4>
        </div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 10, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $pro_quo[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'product_id',
                    'price',
                    'quantity'
                   
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
            <?php foreach ($pro_quo as $i => $proquo): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Producto</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?= $form->field($proquo, "[{$i}]product_id")->dropDownList(
                            ArrayHelper::map(Product::find()->all(),'id','name'),
                            ['prompt'=>'Seleccionar Producto']) 

                        ?>
                      
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($proquo, "[{$i}]price")->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-sm-6">
                            <?= $form->field($proquo, "[{$i}]quantity")->textInput(['maxlength' => true]) ?>
                              </div>
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4><i class="glyphicon glyphicon-envelope"></i> Paquetes</h4>
        </div>
        <div class="panel-body">
            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_package', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items-package', // required: css class selector
                'widgetItem' => '.item-package', // required: css class
                'limit' => 10, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item-package', // css class
                'deleteButton' => '.remove-item-package', // css class
                'model' => $pac_quo[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'package_id'
                   
                ],
            ]); ?>

            <div class="container-items-package"><!-- widgetContainer -->
            <?php foreach ($pac_quo as $i => $pacquo): ?>
                <div class="item-package panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Paquete</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item-package btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item-package btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <?= $form->field($pacquo, "[{$i}]package_id")->dropDownList(
                            ArrayHelper::map(Package::find()->all(),'id','description'),
                            ['prompt'=>'Seleccionar PAquete']) 

                        ?>
                      
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            <?php DynamicFormWidget::end(); ?>
        </div>
    </div>

    
    


    

    

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$form_script = <<< JS
    
        $(document).ready(function () {
            $(".dynamicform_wrapper").on("beforeInsert", function(e, item) {
                console.log("beforeInsert");
            });

            $(".dynamicform_wrapper").on("afterInsert", function(e, item) {
                console.log("afterInsert");
            });

            $(".dynamicform_wrapper").on("beforeDelete", function(e, item) {
                if (! confirm("¿Está seguro que desea eliminar este item?")) {
                    return false;
                }
                return true;
            });

            $(".dynamicform_wrapper").on("afterDelete", function(e) {
                console.log("Item eliminado.");
            });

            $(".dynamicform_wrapper").on("limitReached", function(e, item) {
                alert("Límite de items alcanzado.");
            });
        })


         $(document).ready(function () {
            $(".dynamicform_package").on("beforeInsert", function(e, item) {
                console.log("beforeInsert");
            });

            $(".dynamicform_package").on("afterInsert", function(e, item) {
                console.log("afterInsert");
            });

            $(".dynamicform_package").on("beforeDelete", function(e, item) {
                if (! confirm("¿Está seguro que desea eliminar este item?")) {
                    return false;
                }
                return true;
            });

            $(".dynamicform_package").on("afterDelete", function(e) {
                console.log("Item eliminado.");
            });

            $(".dynamicform_package").on("limitReached", function(e, item) {
                alert("Límite de items alcanzado.");
            });
        })
JS;
$this->registerJs($form_script);
?>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Quotation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cotizacion', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-view">

  

         <div class="grid-view">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Numero de cotizacion</th>
                    <th>Cliente</th>
                    <th>Vendedor</th>
                    <th>Impuesto</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>
                            <?=  $model->id?>
                        </td>
                        <td>
                        <?php
                             $customer = Users::findOne($model->customer_id);
                             echo $customer->name . " " .  $customer->last_name ;

                        ?>
                           
                        </td>
                    
                        <td>
                              <?php
                             $customer = Users::findOne($model->salesman_id);
                             echo $customer->name . " " .  $customer->last_name ;

                        ?>
                         
                        </td>
                        <td>
                        <?=  $model->tax ?>
                        
                        </td>
                       
                    </tr>
             
            </tbody>        
        </table>
    </div>

    <div class="grid-view">
        <div class="summary"><b> Productos </b> </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Descripción</th>
                    <th>Cantidad </th>
                    <th>Precio </th>
                    <th>Sub total Producto </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $index = 0;
                    $total = 0;
                    foreach ($model->productoQuotations as $value) {
                       
                ?>
                    <tr>
                        <td>
                            <?= $index + 1 ?>
                        </td>
                        <td>
                            <?= $value->product->name ?>
                        </td>
                            <td>
                            <?= $value->quantity ?>
                        </td>
                            <td>
                            <?=  $value->price ?>
                        </td>
                        <td>
                            <?php  
                                echo $subtotal = $value->price * $value->quantity;
                             ?>
                        </td>

                       
                    </tr>
                <?php
                        $index++;
                    }
                ?>
            </tbody>        
        </table>
    </div>

        <div class="grid-view">
        <div class="summary"><b> Paquetes </b> </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Descripción</th>
                    <th>Precio </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $index = 0;
                    $total = 0;
                    foreach ($model->packages as $value) {
                     
                       
                ?>
                    <tr>
                        <td>
                            <?= $index + 1 ?>
                        </td>
                        <td>
                            <?= $value->description ?>
                        </td>
                            <td>
                            <?php
                                $subtotal = 0;
                                $total = 0;
                                foreach ($value->promotions as $promotion){
                                    $subtotal = ($promotion->price * $promotion->quantity) - ($promotion->price * $promotion->quantity)*($promotion->discount/100);
                                    $total = $total + $subtotal;


                                }
                                echo $total;
                        ?>
                            </td>
                        
                       

                       
                    </tr>
                <?php
                        $index++;
                    
                }
                ?>
            </tbody>        
        </table>
    </div>

        <div class="grid-view">
        <div class="summary"><b>  </b> </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Subtotal cotizacion </th>
                    <th> Valor del impuesto </th>
                    <th> Total Cotizacion </th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $index = 0;
                    $total = 0;
                   
                     
                       
                ?>
                    <tr>
                        
                            <td>
                            <?php
                                $subtotal = 0;
                                $total = 0;
                                $subtotal_pro = 0;
                                $total_pro = 0;
                                foreach ($model->productoQuotations as $value) {

                                    $subtotal_pro = $value->price * $value->quantity;
                                    foreach ($model->packages as $value) {
                                        foreach ($value->promotions as $promotion){
                                                    $subtotal = ($promotion->price * $promotion->quantity) - ($promotion->price * $promotion->quantity)*($promotion->discount/100);
                                    $total = $total + $subtotal;


                                        }
                                    }
                                    $total_pro += $subtotal_pro;
                                }
                                echo $total + $total_pro;
                        ?>
                            </td>

                               <td>
                            <?php
                                $subtotal = 0;
                                $total = 0;
                                $subtotal_pro = 0;
                                $total_pro = 0;
                                foreach ($model->productoQuotations as $value) {

                                    $subtotal_pro = $value->price * $value->quantity;
                                    foreach ($model->packages as $value) {
                                        foreach ($value->promotions as $promotion){
                                                    $subtotal = ($promotion->price * $promotion->quantity) - ($promotion->price * $promotion->quantity)*($promotion->discount/100);
                                    $total = $total + $subtotal;


                                        }
                                    }
                                    $total_pro += $subtotal_pro;
                                }
                                echo ($total + $total_pro)*$model->tax/100;;
                        ?>
                            </td>

                              <td>
                            <?php
                                $subtotal = 0;
                                $total = 0;
                                $subtotal_pro = 0;
                                $total_pro = 0;
                                foreach ($model->productoQuotations as $value) {

                                    $subtotal_pro = $value->price * $value->quantity;
                                    foreach ($model->packages as $value) {
                                        foreach ($value->promotions as $promotion){
                                                    $subtotal = ($promotion->price * $promotion->quantity) - ($promotion->price * $promotion->quantity)*($promotion->discount/100);
                                    $total = $total + $subtotal;


                                        }
                                    }
                                    $total_pro += $subtotal_pro;
                                }
                                echo  ($total + $total_pro) + ($total + $total_pro)*$model->tax/100;;
                        ?>
                            </td>
                        
                       

                       
                    </tr>
                <?php
                        $index++;
                    
                
                ?>
            </tbody>        
        </table>
    </div>

</div>

</div>

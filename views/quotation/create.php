<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Quotation */

$this->title = 'Create Quotation';
$this->params['breadcrumbs'][] = ['label' => 'Quotations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quotation-create">

    <h1> Crear Cotizacion </h1>

    <?= $this->render('_form', [
        'model' => $model,
        'pro_quo' => $pro_quo,
        'pac_quo' => $pac_quo
    ]) ?>

</div>

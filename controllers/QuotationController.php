<?php

namespace app\controllers;

use Yii;
use app\models\Model;
use app\models\Quotation;
use app\models\ProductoQuotation;
use app\models\DetailQuotation;
use app\models\QuotationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuotationController implements the CRUD actions for Quotation model.
 */
class QuotationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Quotation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new QuotationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Quotation model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Quotation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Quotation();
        $ProductoQuotation = [new ProductoQuotation];
        $PackageQuotation = [new DetailQuotation];

          if ($model->load(Yii::$app->request->post())) {
            
            $ProductoQuotation = Model::createMultiple(ProductoQuotation::classname());
            Model::loadMultiple($ProductoQuotation, Yii::$app->request->post());

            $PackageQuotation = Model::createMultiple(DetailQuotation::classname());
            Model::loadMultiple($PackageQuotation, Yii::$app->request->post());

            $transaction = \Yii::$app->db->beginTransaction();
            try {
                if ($flag = $model->save(false)) {    

                    foreach ($ProductoQuotation as $objProduct) {
                        $objProduct->quotation_id = $model->id;
                        if (! ($flag = $objProduct->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }

                    foreach ( $PackageQuotation as $objPackage) {
                        $objPackage->quotation_id = $model->id;
                        if (! ($flag = $objPackage->save(false))) {
                            $transaction->rollBack();
                            break;
                        }
                    }


                }
                if ($flag) {
                    $model->save(false);
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } catch (Exception $e) {
                $transaction->rollBack();
            }          
        }else{


            return $this->render('create', [
                'model' => $model,
                'pro_quo' => (empty($ProductoQuotation)) ? [new ProductoQuotation] : $ProductoQuotation,
                'pac_quo' => (empty($PackageQuotation)) ? [new DetailQuotation] : $PackageQuotation
            ]);
        }
    }

    /**
     * Updates an existing Quotation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,

            ]);
        }
    }

    /**
     * Deletes an existing Quotation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Quotation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Quotation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Quotation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

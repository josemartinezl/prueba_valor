1) Para correr el la prueba es necesario importar el script de la base de datos que se encuentra dentro de la carpeta 
   DBscsript. Esta ya viene con unos datos de prueba, pero si desean agregar mas datos la aplicacion se los permite.

2) Una vez descargado el repositorio es necesario ejecutar el comando 'composer install' para descargar las dependencias 
   del proyecto.

3) Para el manejo de la prueba se usaron los siguientes widgtes. 
	- https://github.com/wbraganca/yii2-dynamicform
        - https://github.com/kartik-v/yii2-widget-select2
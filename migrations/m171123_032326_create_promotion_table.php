<?php

use yii\db\Migration;

/**
 * Handles the creation of table `promotion`.
 */
class m171123_032326_create_promotion_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('promotion', [
            'product_id' => $this->integer()->unsigned(),
            'package_id' => $this->integer()->unsigned(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->float()->notNull(),
            'discount' => $this->float()->notNull(),
            'PRIMARY KEY(product_id, package_id)',
              ]);

            // creates index for column `product_id`
        $this->createIndex('idx-promotion-product_id','promotion','product_id');

        // add foreign key for table `product`
        $this->addForeignKey(
            'idx-promotion-product_id',
            'promotion',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

                  // creates index for column `package_id`
        $this->createIndex(
            'idx-promotion-package_id',
            'promotion',
            'package_id'
        );

        // add foreign key for table `package`
        $this->addForeignKey(
            'idx-promotion-package_id',
            'promotion',
            'package_id',
            'package',
            'id',
            'CASCADE'
        );


       
      
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('promotion');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `quotation`.
 */
class m171123_041801_create_quotation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('quotation', [
            'id' => $this->primaryKey()->unsigned(),
            'customer_id' => $this->integer()->unsigned(),
            'salesman_id' => $this->integer()->unsigned(),
            'tax' => $this->decimal(10,3)->notNull(),
            ]);


        // creates index for column `customer_id`
        $this->createIndex(
            'idx-quotation-customer_id',
            'quotation',
            'customer_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'idf-quotation-customer_id',
            'quotation',
            'customer_id',
            'users',
            'id',
            'CASCADE'
        );
             // creates index for column `salesman_id`
        $this->createIndex(
            'idx-quotation-salesman_id',
            'quotation',
            'salesman_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'idf-quotation-salesman_id',
            'quotation',
            'salesman_id',
            'users',
            'id',
            'CASCADE'
        );




    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('quotation');
    }
}

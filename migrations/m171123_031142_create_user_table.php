<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171123_031142_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey()->unsigned(),
            'ruc' => $this->string(50)->notNull()->unique(),
            'name' => $this->string(50)->notNull(),
            'last_name' => $this->string(50)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}

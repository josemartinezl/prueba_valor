<?php

use yii\db\Migration;

/**
 * Handles the creation of table `producto_quotation`.
 */
class m171128_192304_create_producto_quotation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('producto_quotation', [
            'product_id' => $this->integer()->unsigned(),
            'quotation_id' => $this->integer()->unsigned(),
            'price' => $this->decimal(10,2)->notNull(),
            'quantity' => $this->integer()->notNull(),
            'PRIMARY KEY(product_id,quotation_id)',
        ]);


            // creates index for column `product_id`
        $this->createIndex('idx-quotation-product_id','producto_quotation','product_id');

        // add foreign key for table `product`
        $this->addForeignKey(
            'idf-quotation_product_id',
            'producto_quotation',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        // creates index for column `quotitation_id`
        $this->createIndex(
            'idx-quotation-quotation_id',
            'producto_quotation',
            'quotation_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'idf-quotation-quotation_id',
            'producto_quotation',
            'quotation_id',
            'quotation',
            'id',
            'CASCADE'
        );
 



    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('producto_quotation');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `detail_quotation`.
 */
class m171123_044041_create_detail_quotation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('detail_quotation', [
            'package_id' => $this->integer()->unsigned(),
            'quotation_id' => $this->integer()->unsigned(),
            'PRIMARY KEY(package_id, quotation_id)',
        ]);

            // creates index for column `quotation_id`
        $this->createIndex('idx-detail_quotation-quotation_id','detail_quotation','quotation_id');

        // add foreign key for table `product`
        $this->addForeignKey(
            'idf-detail_quotation-quotation_id',
            'detail_quotation',
            'quotation_id',
            'quotation',
            'id',
            'CASCADE'
        );


        // creates index for column `promotion_package_id`
        $this->createIndex(
            'idx-detail_quotation_package_id',
            'detail_quotation',
            'package_id'
        );

        // add foreign key for table `promotion`
        $this->addForeignKey(
            'idf-detail_quotation_package_id',
            'detail_quotation',
            'package_id',
            'package',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('detail_quotation');
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `package`.
 */
class m171123_032148_create_package_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('package', [
            'id' => $this->primaryKey()->unsigned(),
            'description' => $this->string(50)->notNull(),

            
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('package');
    }
}
